#!/usr/bin/env bash

# ALPHA version for checking POODLE tls

# bash socket implementation of checking the availability of TLS, (SSLv3 to TLS 1.2)
# Based on my bash-heartbleed (loosely based on my bash-heartbleed implementation)
#
# Author: Dirk Wetter, GPLv2 see https://testssl.sh/LICENSE.txt 



TMPfile=`mktemp /tmp/poodletls.$1..XXXXXX` || exit 6

/usr/bin/openssl s_client -debug -connect  $1:443 -fallback_scsv -no_tls1_2  </dev/null &>$TMPfile
ret=$?

if grep -q "tlsv1 alert inappropriate fallback" $TMPfile; then
	tput setaf 2
	echo "likely not vulnerable (OK) -- $ret"
fi
if grep -q "Certificate chain" $TMPfile; then
	tput setaf 1
	echo "seems vulnerable (NOT ok) -- $ret"
fi

rm $TMPfile

exit 0




